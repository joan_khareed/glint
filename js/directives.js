glint.directive('contactData',function(){
    return {
        restrict: "E",
        templateUrl: "pages/directive.htm",
        replace: true,
        scope: {
            name: "@",
            email: "@",
            subject: "@",
            message: "@"
        }
    }
})