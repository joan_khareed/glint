//controllers

glint.controller('homeController',['$scope','pageService',function($scope,pageService){
    pageService.page = "Home Page";
    $scope.page = pageService.page;
}]);

glint.controller('aboutController',['$scope','$http','pageService',function($scope,$http,pageService){
    pageService.page = "About Page";
    $scope.page = pageService.page;
    
    $http({
            method: 'get',
            url: "http:glint-backend.com/api/about",
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function successCallback(response) {
            $scope.aboutResult=response.data;
        });
}]);

glint.controller('serviceController',['$scope','$http','pageService',function($scope,$http,pageService){ 
    pageService.page = "Service Page";
    $scope.page = pageService.page;
    
     $http({
            method: 'get',
            url: "http:glint-backend.com/api/services",
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function successCallback(response) {
            $scope.serviceResult=response.data;
        });
}]);

glint.controller('worksController',['$scope','pageService',function($scope,pageService){
    pageService.page = "Works Page";
    $scope.page = pageService.page;
}]);

glint.controller('clientsController',['$scope','pageService',function($scope,pageService){ 
    pageService.page = "Clients Page";
    $scope.page = pageService.page;
}]);

glint.controller('contactController',['$scope','pageService','contactDataService',function($scope,pageService,contactDataService){ 
    pageService.page = "Contact Page";
    $scope.page = pageService.page;
    
    $scope.$watch('name', function(){
        contactDataService.name = $scope.name;
    });
    $scope.$watch('email',function(){
        contactDataService.email = $scope.email;
    });
    
    $scope.$watch('subject',function(){
        contactDataService.subject = $scope.subject;
    });
    $scope.$watch('message',function(){
        contactDataService.message = $scope.message;
    });
}]);

glint.controller('contactSubmitController', ['$scope','contactDataService',function($scope,contactDataService){
    $scope.name = contactDataService.name;
    $scope.email = contactDataService.email;
    $scope.subject = contactDataService.subject;
    $scope.message = contactDataService.message;
}]);