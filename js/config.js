glint.config(function($routeProvider){
    
    $routeProvider
    .when('/',{
        templateUrl : 'pages/home.htm',
        controller : 'homeController'
    })
    
    .when('/about',{
        templateUrl : 'pages/about.htm',
        controller : 'aboutController'
    })
    
    .when('/services',{
        templateUrl : 'pages/services.htm',
        controller : 'serviceController'
    })
    .when('/works',{
        templateUrl : 'pages/works.htm',
        controller : 'worksController'
    })
    .when('/clients',{
        templateUrl : 'pages/clients.htm',
        controller :  'clientsController'
    })
    
    .when('/contact',{
        templateUrl : 'pages/contact.htm',
        controller : 'contactController'
    })
    .when('/contactSubmit',{
        templateUrl : 'pages/data.htm',
        controller : 'contactSubmitController' 
    })
});

glint.config(['$locationProvider', function($locationProvider) {
  $locationProvider.hashPrefix('');
}]);